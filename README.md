# Tickets

Tickets is a fullstack webapp built in Go that interacts with the stubhub API to measure ticket prices vs face
value ratio to determine if a sale is a good deal.

The app is available for viewing [here](http://107.191.39.147/)
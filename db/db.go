//Package db manages the mongoDB queries and insertions
package db

import (
	"fmt"

	"github.com/nickshater/tickets/types"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//GetSession creates a mongoDB session
func GetSession() *mgo.Session {
	s, err := mgo.Dial("mongodb://localhost")
	if err != nil {
		fmt.Println("mongo session error", err)
	}
	return s
}

//GetQueryFromDB returns the search query for a db id
func GetQueryFromDB(id int) string {
	s := GetSession()
	defer s.Close()

	c := s.DB("tickets").C("stadiums")
	var result struct {
		Query string `bson:"query"`
	}

	err := c.Find(bson.M{"id": id}).Select(bson.M{"query": 1}).One(&result)
	if err != nil {
		fmt.Println("Get Query error ", err)
	}
	return result.Query
}

//ReturnAllDeals returns all db results for teams next deals
func ReturnAllDeals() []types.TeamDeals {
	s := GetSession()
	defer s.Close()

	c := s.DB("tickets").C("stadiums")

	var results []types.TeamDeals
	err := c.Find(nil).Sort("team").All(&results)
	if err != nil {
		fmt.Println("ReturnAllDeals error", err)
	}
	return results
}

//UpdateTeamDeal updates the top deal record for a team
func UpdateTeamDeal(id int, r types.SearchResultSummary) {
	s := GetSession()
	defer s.Close()

	c := s.DB("tickets").C("stadiums")
	input := bson.M{"$set": bson.M{"totalTickets": r.Listings[0].TotalTickets, "avgPrice": r.Listings[0].AvgPrice, "lowPrice": r.Listings[0].LowPrice, "valueRatio": r.Listings[0].Face.BestRatio, "avgValueRatio": r.Listings[0].Face.AvgRatio, "valuePrice": r.Listings[0].Face.BestPrice, "valueFace": r.Listings[0].Face.BestFace, "eventURL": r.Events[0].WebURI, "valueURL": r.Listings[0].Face.BestListing, "eventName": r.Events[0].Name, "eventDate": r.Events[0].DateTime}}
	err := c.Update(bson.M{"id": id}, input)
	if err != nil {
		fmt.Println("UpdateTeamDeal error ", err)
	}
}

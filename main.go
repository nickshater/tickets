//Tickets is a web app to locate high value tickets on stubhub using the stubhub API
package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/nickshater/tickets/stubhub"
	"github.com/nickshater/tickets/web"
)

func main() {
	// Uncomment to use the CLI interface
	// space := "%20"
	// var q string

	// for i := 1; i < len(os.Args); i++ {
	// 	if i > 1 {
	// 		q += space
	// 	}
	// 	q += os.Args[i]
	// }

	// fmt.Println("starting")
	// stubhub.CLIResults(q)
	// fmt.Println("\nfinished")

	//Comment out below this line and uncomment above for CLI interface
	go stubhub.LoopThroughTeams()
	web.PopulateTemplates()
	http.HandleFunc("/", web.SearchHandler)
	http.HandleFunc("/results/", web.ResultsHandler)
	http.HandleFunc("/teams/", web.DealsHandler)
	http.Handle("/static/", http.FileServer(http.Dir("public")))
	fmt.Println("Running Server On Port :6969")
	log.Fatal(http.ListenAndServe(":6969", nil))

}

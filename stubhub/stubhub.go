//Package stubhub manages stubhub API calls and manipulates returned data for display
package stubhub

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/nickshater/tickets/db"
	"github.com/nickshater/tickets/types"
)

//SearchForEvent searches the stubhub API for an event
func SearchForEvent(query string) (types.SearchResultSummary, error) {
	var uri = "https://api.stubhub.com/search/catalog/events/v3?status=active&sort=eventDateLocal%20asc&parking=false&q=" + query
	client := &http.Client{}

	key := os.Getenv("STUBHUB")

	request, err := http.NewRequest("GET", uri, nil)
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	request.Header.Set("Authorization", key)
	request.Header.Set("Accept", "application/json")
	request.Header.Set("Accept-Encoding", "application/json")
	resp, err := client.Do(request)

	if err != nil {
		fmt.Println("error", err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("ioutil err", err)
	}

	defer resp.Body.Close()

	var e types.SearchResult
	err = json.Unmarshal(body, &e)
	if err != nil {
		fmt.Println("error with json", err)
	}

	var s types.SearchResultSummary

	if len(e.Events) > 4 {
		for i := 0; i < 5; i++ {
			s.Events = append(s.Events, e.Events[i])
			l := GetListingsByEvent(e.Events[i].ID)
			s.Listings = append(s.Listings, l)
		}
	} else if len(e.Events) > 0 {
		for i := 0; i < len(e.Events); i++ {
			s.Events = append(s.Events, e.Events[i])
			l := GetListingsByEvent(e.Events[i].ID)
			s.Listings = append(s.Listings, l)
		}
	} else {
		s.Empty = true
	}
	return s, err
}

//NextGame fills the database with the next upcoming result
func NextGame(id int) {
	var query = db.GetQueryFromDB(id)
	clean := strings.Replace(query, " ", "%20", -1)
	var uri = "https://api.stubhub.com/search/catalog/events/v3?status=active&sort=eventDateLocal%20asc&parking=false&q=" + clean

	client := &http.Client{}

	key := os.Getenv("STUBHUB")

	request, err := http.NewRequest("GET", uri, nil)
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	request.Header.Set("Authorization", key)
	request.Header.Set("Accept", "application/json")
	request.Header.Set("Accept-Encoding", "application/json")
	resp, err := client.Do(request)

	if err != nil {
		fmt.Println("error", err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("ioutil err", err)
	}

	defer resp.Body.Close()

	var e types.SearchResult
	err = json.Unmarshal(body, &e)
	if err != nil {
		fmt.Println("error with json", err)
	}

	var s types.SearchResultSummary
	if len(e.Events) != 0 {
		s.Events = append(s.Events, e.Events[0])
		l := GetListingsByEvent(e.Events[0].ID)
		s.Listings = append(s.Listings, l)
	} else {
		s.Empty = true
	}
	db.UpdateTeamDeal(id, s)
}

//LoopThroughTeams loops through the stadiums db and updates the teams best deal
func LoopThroughTeams() {

	count := 1

	for {
		NextGame(count)
		time.Sleep(1 * time.Minute)
		count++
		if count == 31 {
			count = 1
		}
	}
}

//GetListingsByEvent searches for tickets by event ID
func GetListingsByEvent(event int) types.ListingsResultSummary {
	ev := strconv.Itoa(event)
	var uri = fmt.Sprint("https://api.stubhub.com/search/inventory/v2?eventid=" + ev + "&pricingsummary=true&quantity=2&limit=1000&start=0&rows=1000")
	client := &http.Client{}

	key := os.Getenv("STUBHUB")

	request, err := http.NewRequest("GET", uri, nil)
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	request.Header.Set("Authorization", key)
	request.Header.Set("Accept", "application/json")
	request.Header.Set("Accept-Encoding", "application/json")
	resp, err := client.Do(request)

	if err != nil {
		fmt.Println("error", err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("ioutil err", err)
	}

	defer resp.Body.Close()

	var e = new(types.EventResults)
	err = json.Unmarshal(body, &e)
	if err != nil {
		fmt.Println("error with json", err)
	}

	var l types.ListingsResultSummary

	l.LowPrice = e.Summary.MinPrice
	l.AvgPrice = e.Summary.AvgPrice
	l.TotalTickets = e.TotalTickets

	l.Face = BetterThanFace(e.Listings)
	return l
}

//BetterThanFace compares listings to face value for best deal
func BetterThanFace(listings []types.Listings) types.FaceResultSummary {
	best := 10.0
	var ratio, ratSum, ratCnt float64
	var r types.FaceResultSummary

	for i := 0; i < len(listings); i++ {

		if listings[i].FaceValue.Amount != 0 {
			ratio = (listings[i].Price.Amount / listings[i].FaceValue.Amount)
			ratSum += ratio
			ratCnt++
			if ratio < best {
				best = ratio
				r.BestRatio = ratio
				r.BestListing = listings[i].ListingID
				r.BestPrice = listings[i].Price.Amount
				r.BestFace = listings[i].FaceValue.Amount
			}
		}
		r.AvgRatio = ratSum / ratCnt
	}
	return r
}

//CLIResults returns the results in the Command line instead of on the server
func CLIResults(query string) error {
	c, err := SearchForEvent(query)

	if err != nil {
		return err
	}

	for i := 0; i < 5; i++ {
		fmt.Println("\nResult", (i + 1), c.Events[i].Name, c.Events[i].DateTime)
		fmt.Println("Event ID", c.Events[i].ID)
		fmt.Println("URL", c.Events[i].WebURI)
		fmt.Println("Low Price", c.Listings[i].LowPrice)
		fmt.Printf("Avg Price %.2f \n", c.Listings[i].AvgPrice)
		fmt.Println("TotalTickets", c.Listings[i].TotalTickets)
		if c.Listings[i].Face.BestFace == 0 {
			fmt.Println("No Face Value Information Available")
		} else {
			fmt.Printf("Ratio %.2f \n", c.Listings[i].Face.BestRatio)
			fmt.Println("Listing ID", c.Listings[i].Face.BestListing)
			fmt.Printf("Face Val %.2f \n", c.Listings[i].Face.BestFace)
			fmt.Println("Price", c.Listings[i].Face.BestPrice)
			fmt.Printf("Avg Ratio %.2f \n", c.Listings[i].Face.AvgRatio)
		}
	}
	return err
}

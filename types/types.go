//Package types stores the types for API results and output formatting
package types

//SearchResult is a type for the event search result
type SearchResult struct {
	Num    int     `json:"numFound"`
	Events []Event `json:"events"`
}

//Event is a type for events returned from a search
type Event struct {
	ID       int     `json:"id"`
	Name     string  `json:"name"`
	WebURI   string  `json:"webURI"`
	DateTime string  `json:"eventDateLocal"`
	Venue    Venue   `json:"venue"`
	Tickets  Tickets `json:"ticketInfo"`
}

//Venue is data for a venue
type Venue struct {
	ID     int    `json:"id"`
	Name   string `json:"name"`
	WebURI string `json:"webURI"`
	City   string `json:"city"`
}

//Tickets shows high and low ticket price for an event
type Tickets struct {
	Low   float64 `json:"minPrice"`
	High  float64 `json:"maxPrice"`
	Total int     `json:"totalTickets"`
}

//EventResults return info when searching by event ID
type EventResults struct {
	TotalListings int        `json:"totalListings"`
	TotalTickets  int        `json:"totalTickets"`
	Listings      []Listings `json:"listing"`
	Summary       PricingSum `json:"pricingSummary"`
}

//Listings is an object in an array of ticket listings
type Listings struct {
	ListingID int   `json:"listingId"`
	Price     Price `json:"currentPrice"`
	Section   int   `json:"sectionId"`
	FaceValue Price `json:"faceValue"`
}

//PricingSum will show an overview of pricing for an event
type PricingSum struct {
	MinPrice float64 `json:"minTicketPrice"`
	MaxPrice float64 `json:"maxTicketPrice"`
	AvgPrice float64 `json:"averageTicketPrice"`
}

//Price is a price object
type Price struct {
	Amount   float64 `json:"amount"`
	Currency string  `json:"currency"`
}

//SearchResultSummary is the return data after the main search function is run
type SearchResultSummary struct {
	Events   []Event
	Listings []ListingsResultSummary
	Empty    bool
}

// ListingsResultSummary is the return data for Listing Search
type ListingsResultSummary struct {
	LowPrice     float64
	AvgPrice     float64
	TotalTickets int
	Face         FaceResultSummary
}

//FaceResultSummary is the return data for the face FaceValue logic
type FaceResultSummary struct {
	BestRatio   float64
	BestListing int
	BestFace    float64
	BestPrice   float64
	AvgRatio    float64
}

// ResultsCard merges SearchResultSummary to work with in template
type ResultsCard struct {
	Name         string
	WebURI       string
	DateTime     string
	BestRatio    float64
	BestListing  int
	BestFace     float64
	BestPrice    float64
	AvgRatio     float64
	LowPrice     float64
	AvgPrice     float64
	TotalTickets int
	ValueResults bool
	NoValue      bool
	Team         string
}

//TeamDeals is the type for a db collection return
type TeamDeals struct {
	Team          string  `bson:"team"`
	TotalTickets  int     `bson:"totalTickets"`
	AvgPrice      float64 `bson:"avgPrice"`
	LowPrice      float64 `bson:"lowPrice"`
	BestRatio     float64 `bson:"valueRatio"`
	AvgRatio      float64 `bson:"avgValueRatio"`
	BestPrice     float64 `bson:"valuePrice"`
	BestFace      float64 `bson:"valueFace"`
	WebURI        string  `bson:"eventURL"`
	BestListingID int     `bson:"valueURL"`
	EventName     string  `bson:"eventName"`
	EventDate     string  `bson:"eventDate"`
}

//Package web manages the handle functions for GUI management
package web

import (
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	"os"
	"strings"
	"text/template"

	"github.com/nickshater/tickets/db"
	"github.com/nickshater/tickets/stubhub"
	"github.com/nickshater/tickets/types"
)

var resultsTemplate *template.Template
var noResultsTemplate *template.Template
var searchTemplate *template.Template
var teamsTemplate *template.Template
var templates *template.Template
var err error

//PopulateTemplates parses all templates in the templates folder
func PopulateTemplates() {
	var allFiles []string
	templatesDir := "./templates/"
	files, err := ioutil.ReadDir(templatesDir)
	if err != nil {
		fmt.Println("Error reading template dir")
		os.Exit(1)
	}
	for _, file := range files {
		filename := file.Name()
		if strings.HasSuffix(filename, ".html") {
			allFiles = append(allFiles, templatesDir+filename)
		}
	}
	if err != nil {
		fmt.Println("first error\n\n\n", err)
		os.Exit(1)
	}
	templates, err = template.ParseFiles(allFiles...)
	if err != nil {
		fmt.Println("second error\n\n", err)
		os.Exit(1)
	}
	noResultsTemplate = templates.Lookup("noresults.html")
	resultsTemplate = templates.Lookup("results.html")
	searchTemplate = templates.Lookup("search.html")
	teamsTemplate = templates.Lookup("teams.html")
}

//SearchHandler is the home/search page
func SearchHandler(w http.ResponseWriter, r *http.Request) {
	searchTemplate.Execute(w, nil)
	if err != nil {
		fmt.Println(err)
	}
}

//ResultsHandler returns the event search results
func ResultsHandler(w http.ResponseWriter, r *http.Request) {
	query := r.FormValue("search")
	clean := strings.Replace(query, " ", "%20", -1)

	results, err := stubhub.SearchForEvent(clean)
	if err != nil {
		fmt.Println("error at web results")
	}

	if results.Empty == true {
		noResultsTemplate.Execute(w, nil)
	} else {
		c := make([]types.ResultsCard, len(results.Events))

		for i := 0; i < len(results.Events); i++ {
			c[i].Name = results.Events[i].Name
			c[i].WebURI = results.Events[i].WebURI
			c[i].DateTime = results.Events[i].DateTime
			c[i].BestRatio = results.Listings[i].Face.BestRatio
			c[i].BestListing = results.Listings[i].Face.BestListing
			c[i].BestFace = results.Listings[i].Face.BestFace
			c[i].BestPrice = results.Listings[i].Face.BestPrice
			c[i].AvgRatio = results.Listings[i].Face.AvgRatio
			c[i].LowPrice = results.Listings[i].LowPrice
			c[i].AvgPrice = results.Listings[i].AvgPrice
			c[i].TotalTickets = results.Listings[i].TotalTickets
			if math.IsNaN(c[i].AvgRatio) == true {
				c[i].NoValue = true
			}
		}
		resultsTemplate.Execute(w, c)
	}

	if err != nil {
		fmt.Println(err)
	}
}

//DealsHandler Collects deals from DB and returns the results to the teams template
func DealsHandler(w http.ResponseWriter, r *http.Request) {
	d := db.ReturnAllDeals()

	c := make([]types.ResultsCard, len(d))
	for i := 0; i < len(d); i++ {
		c[i].Name = d[i].EventName
		c[i].WebURI = d[i].WebURI
		c[i].DateTime = d[i].EventDate
		c[i].BestRatio = d[i].BestRatio
		c[i].BestListing = d[i].BestListingID
		c[i].BestFace = d[i].BestFace
		c[i].BestPrice = d[i].BestPrice
		c[i].AvgRatio = d[i].AvgRatio
		c[i].LowPrice = d[i].LowPrice
		c[i].AvgPrice = d[i].AvgPrice
		c[i].TotalTickets = d[i].TotalTickets
		c[i].Team = d[i].Team
		if math.IsNaN(c[i].AvgRatio) == true {
			c[i].NoValue = true
		}
	}

	teamsTemplate.Execute(w, c)

}
